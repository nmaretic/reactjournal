using System;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NewSite.Web.Models;
using NewSite.Web.Services;

namespace NewSite.Web.Controllers
{
    [Route("api/notes")]
    public class NotesController : Controller
    {
        private IJournalRepository _journalRepository;

        public NotesController(IJournalRepository journalRepository)
        {
            _journalRepository = journalRepository;
        }
        [HttpPost()]
        public IActionResult CreateNote([FromBody] NoteDto note)
        {
            if (note == null)
            {
                return BadRequest();
            }

            var noteEntity = Mapper.Map<Note>(note);
            _journalRepository.AddNote(noteEntity);
            if (!_journalRepository.Save())
            {
                throw new Exception("Creating note failed on save.");
            }

            var noteToReturn = Mapper.Map<NoteDto>(noteEntity);
            return CreatedAtRoute("GetNote", new {id = noteToReturn.Id}, noteToReturn);
        }

        [HttpGet("{id}", Name = "GetNote")]
        public IActionResult GetNote(Guid id)
        {
            var note = _journalRepository.GetNote(id);
            return Ok(note);
        }

        [HttpGet()]
        public IActionResult GetNotes()
        {
            var notes = _journalRepository.GetNotes();
            //pagination
            return Ok(notes);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateNote(Guid id, [FromBody] NoteDto noteUpdate)
        {
            if (noteUpdate == null)
            {
                return BadRequest();
            }
            var note = _journalRepository.GetNote(id);
            if (note == null)
            {
                return NotFound();
            }
            //refactor
            note.Title = noteUpdate.Title;
            note.Body = noteUpdate.Body;
            note.StateOfMind = noteUpdate.StateOfMind;
            _journalRepository.UpdateNote(note);
            if (!_journalRepository.Save())
            {
                throw new Exception($"Could not update note {note.Id}. Failed on save");
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteNote(Guid id)
        {
            var note = _journalRepository.GetNote(id);
            if (note == null)
            {
                return NotFound();
            }
            _journalRepository.DeleteNote(note);
            return NoContent();
        }

    }
}