using System;

namespace NewSite.Web.Models
{
    public class NoteDto
    {
        public Guid Id { get; set; }
        public DateTime CreateTime { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public StateOfMind? StateOfMind { get; set; }

    }
}