﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewSite.Web.Entities;
using NewSite.Web.Models;
using SQLitePCL;

namespace NewSite.Web.Services
{
    public class JournalRepository : IJournalRepository
    {
        private JournalContext _context;
        public JournalRepository(JournalContext context)
        {
            _context = context;
        }

        public void AddNote(Note note)
        {
            note.Id = new Guid();
            _context.Notes.Add(note);
        }
        public Note GetNote(Guid id)
        {
            return _context.Notes.FirstOrDefault(n => n.Id == id);
        }
        public IEnumerable<Note> GetNotes()
        {
            return _context.Notes
                .OrderBy(n => n.CreateTime)
                .ToList();
        }

        public void UpdateNote(Note note)
        {
            _context.Update(note);
        }

        public void DeleteNote(Note note)
        {
            _context.Notes.Remove(note);
            _context.SaveChanges();
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}
