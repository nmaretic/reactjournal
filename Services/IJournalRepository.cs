﻿using System;
using System.Collections.Generic;
using NewSite.Web.Models;

namespace NewSite.Web.Services
{
    public interface IJournalRepository
    {
        void AddNote(Note note);
        void DeleteNote(Note note);
        Note GetNote(Guid id);
        IEnumerable<Note> GetNotes();
        void UpdateNote(Note note);
        bool Save();
    }
}