import React from 'react';

const editNote = (props) => {
    return props.note == null ? null : ( 
        <form className="editNote">
            <div className="form-group">
                <label >Title</label>
                <input type="text" 
                    className="form-control" 
                    placeholder="Title" 
                    value={props.note.title}
                    onChange={props.titleChanged} />   
            </div>
            <div className="form-group">
                <label>Note</label>
                <textarea className="form-control" 
                    rows="6" 
                    id="inputBody" 
                    placeholder="Write Something..." 
                    value={props.note.body}
                    onChange={props.bodyChanged}/>  
            </div>
            <label className="radio-inline">
                <input type="radio" 
                    name="inlineRadioOptions"
                    checked={props.note.StateOfMind === '0'}  
                    value="0"/> 0
            </label>
            <label className="radio-inline">
                <input type="radio" name="inlineRadioOptions" 
                    checked={props.note.StateOfMind === '1'}  
                    value="1"/> 1
                    </label>
            <label className="radio-inline">
                <input type="radio" 
                    name="inlineRadioOptions"
                    checked={props.note.StateOfMind === '2'}  
                    value="2"/> 2
            </label>
            <label className="radio-inline">
                <input type="radio" 
                    name="inlineRadioOptions"
                    checked={props.note.StateOfMind === '3'}  
                    value="3"/> 3
            </label>
            <br/>
            <button className="btn btn-info" type="submit">Save</button>
        </form>
    );
}
 
export default editNote;