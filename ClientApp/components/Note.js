import React from 'react';
import { Link } from 'react-router-dom';

const note = (props) => {
    return ( 
        <div className="note">
            <h3>{props.note.title}</h3>
            <p>{props.note.createTime}</p>
            <p>{props.note.body}</p>
            {/* <Link to={{
                pathname: `/edit/${props.note.id}`,
                state: {
                    note: props.note
                }
            }}> */}
                <button className="btn btn-success" onClick={props.edit}>Edit</button>
            {/* </Link> */}
            <button className="btn btn-danger" onClick={props.delete}>Delete</button>
        </div>
    );
}
 
export default note;