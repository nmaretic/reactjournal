import React, { Component } from "react";
import axios from 'axios';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'
import Modal from './UI/Modal'; 
import EditNote from './EditNote';
import Note from './Note';
import { timingSafeEqual } from "crypto";

export class Notes extends Component {
  state = {
    notes : [],
    modalIsOpen : false,
    noteForEdit : null
  }
  componentDidMount() {
    axios.get("/api/notes")
      .then(response => {
        this.setState({notes : response.data});
      });
  }
  onDelete = (note) => {
    confirmAlert({
      title: 'Delete ' ,
      message: 'Are you sure you want to delete ' + note.title + '?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            axios.delete("/api/notes/" + note.id)
              .then(response => {
                if(response.status == 204)
                  axios.get("api/notes")
                    .then(response => this.setState({notes : response.data}));  
              })
          }
        },
        {
          label: 'No'
        }
      ]
    });
  }
  onEdit = (note) => {
    //   const index = this.state.notes.findIndex(n => n.id == note.id)
    //   let notes = this.state.notes;
    //   notes[index] = note;
    //   this.setState({notes : notes})
      let noteForEdit = {};
      Object.assign(noteForEdit,note)
      debugger;
      this.setState({modalIsOpen : true, noteForEdit : noteForEdit});
  }
  handleSubmit = () =>{
      axios.put("/api/notes/" + this.state.noteForEdit.id,
      this.state.noteForEdit)
        .then(response => console.log("you put it well"))
        .catch(error => console.log(error))
  }
  titleChanged = (event) => {
    let title = event.target.value;
    this.setState(prevState => ({
        noteForEdit: {
            ...prevState.noteForEdit,
            title
        }
    }))
  }

  bodyChanged = () => {

  }
  render() {
    return (
        <div>
            <Modal show={this.state.modalIsOpen}>
                <EditNote 
                    titleChanged={this.titleChanged}
                    bodyChanged={this.bodyChanged}
                    note={this.state.noteForEdit}
                    handleSubmit={this.handleSubmit}/>
            </Modal>

            {this.state.notes.map(n => (
                <Note
                  key={n.id} 
                  note={n}
                  edit={() => this.onEdit(n)}
                  delete={() => this.onDelete(n)}
            />))}
        </div>
    )
  }
}
