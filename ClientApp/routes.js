import React, { Component } from "react";
import { Route } from "react-router-dom";
import { Layout } from "./components/Layout";
import { Home } from "./components/Home";
import EditNote from "./components/EditNote";

export const routes = (
  <Layout>
    <Route exact path="/" component={Home} />
    <Route exact path="/edit/:id" component={EditNote} />
  </Layout>
);
