﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewSite.Web.Models
{
    public class Note
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreateTime { get; set; }

        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        public string Body { get; set; }

        public StateOfMind? StateOfMind { get; set; }
    }

    public enum StateOfMind
    {
        Sad, Angry, Happy, Excited
    }
}
