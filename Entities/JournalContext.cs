﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NewSite.Web.Models;

namespace NewSite.Web.Entities
{
    public class JournalContext : DbContext
    {
        public JournalContext(DbContextOptions<JournalContext> options)
            : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Note> Notes { get; set; }
    }
}
